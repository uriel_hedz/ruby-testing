class Calculator
	attr_accessor :x,:y
	def initialize(x,y)
		@x = x.to_f
		@y = y.to_f
	end

	def sum
		@x + @y
	end

	def multiply
	end

	def divide
		raise ZeroDivisionError if @y == 0
		@x / @y
	end



end