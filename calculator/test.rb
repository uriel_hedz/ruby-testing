require "./calculator"
require "minitest/autorun"


class TestCalculator < MiniTest::Test
	def setup
		@calculator = Calculator.new(1,2)
	end

	def test_calculator_can_sum
		assert_equal 3, @calculator.sum
	end

	def test_calculator_has_a_multiply_method
		assert_respond_to @calculator, :multiply
	end

	def test_calculator_can_divide
		assert_equal (1.to_f/2.to_f), @calculator.divide
	end

	def test_assert_fail_if_divided_by_zero
		assert_raises ZeroDivisionError do
			@calculator.y = 0
			@calculator.divide
		end
	end

	def test_calculator_division_returns_float
		assert_instance_of Float, @calculator.divide
	end


end